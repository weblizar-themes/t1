<?php 
function transit_x_color_customize_register( $wp_customize ) {

		$wp_customize->add_section(
		    'color_section',
		        array(
		            'title' => 'Color Section',
		            'description' => 'Here you can add color',
					'panel'=>'Wheelify_Theme_Custom_Add_Panel_Function',
					'capability'=>'edit_theme_options',
		            'priority' => 5,
		));

		$wp_customize->add_setting( 'transit_x_theme_color', array(
			'sanitize_callback' => 'transpo_x_Sanitize_Text_Function',
		) );

		$wp_customize->add_control( new WP_Customize_Control(
		    $wp_customize,
			'transit_x_theme_color',
			    array(
			        'label'    => __( ' Color ', 'transit-x' ),
			        'section'  => 'color_section',
			        'settings' => 'transit_x_theme_color',
			        'type'     => 'select',
			      
                  'choices'  => array(
                  'default-color'      => __( 'Default Color', 'transit-x' ),	
                  'red'      => __( 'red', 'transit-x' ),
                  'green'    => __( 'green', 'transit-x' ),
                  'blue'     => __( 'blue', 'transit-x' ),
              ) )
		    )
		);

		// Sanitize Text Function 
		function transpo_x_Sanitize_Text_Function( $text ) {
		    return sanitize_text_field( $text );
		}

    }
add_action( 'customize_register', 'transit_x_color_customize_register' );