=== Transit-X ===
Contributors: weblizar
Requires at least: 5.0
Tested up to: 5.8
Requires PHP: 5.6
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags:  

Transit-X WordPress Free theme under GPL V2 

== Description ==

Description. 

== Frequently Asked Questions ==

= How to Install Theme =

1. In your admin panel, go to Appearance > Themes and click the Add New button and Search for blog starter, OR
2. If you have theme zip file, click Upload and choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Copyright ==

* Transit-X WordPress Theme, Copyright 2021
* Transit-X is distributed under the terms of the GNU GPL

Transit-X Theme the following third-party resources:

== License/copyright for images ==
== Image Resources ==
= Screenshot =
* https://pxhere.com/en/photo/1347515 | CC0 Public Domain
* All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)



